const tasks = {
    main: 'sass',
    watch: 'watch-sass',
    clean: 'clean-sass'
};

function initialize(srcPath, dstPath, sassOptions) {
    if (!srcPath) { throw 'Source Path is required.'; }
    if (!dstPath) { throw 'Destination Path is required.'; }

    const path = require('path');
    const gulp = require('gulp');
    const sass = require('gulp-sass');
    const postcss = require('gulp-postcss');
    const sourcemaps = require('gulp-sourcemaps');
    const rewriteCss = require('gulp-rewrite-css');
    const autoprefixer = require('autoprefixer');
    const del = require('del');

    const compiledDir = path.join(dstPath, 'Compiled');

    gulp.task(tasks.main, function () {
        return gulp.src(srcPath)
            .pipe(sourcemaps.init())
            .pipe(sass(sassOptions))
            .pipe(postcss([autoprefixer({
                overrideBrowserslist: [
                    'last 10 versions',
                    '> 5%'
                ]
            })]))
            .pipe(sourcemaps.write())
            .pipe(rewriteCss({
                destination: getRewriteCssDestPath(compiledDir)
            }))
            .pipe(gulp.dest(compiledDir));
    });
    gulp.task(tasks.watch, function () {
        gulp.watch(srcPath, { ignoreInitial: false }, gulp.series(tasks.main));
    });
    gulp.task(tasks.clean, function () {
        return del([
            `${compiledDir}/**/*`
        ]);
    });

    function getRewriteCssDestPath(dstPath) {
        const normalizedPath = path.normalize(dstPath);
        if (normalizedPath.startsWith('wwwroot')) {
            return normalizedPath.split(path.sep).slice(1).join(path.sep);
        }

        return dstPath;
    }
}

module.exports = initialize;
module.exports.tasks = tasks;
