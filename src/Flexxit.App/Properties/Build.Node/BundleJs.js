const tasks = {
    main: 'bundle-js',
    noop: 'bundle-js-noop',
    watchNoop: 'watch-bundle-js-noop',
    clean: 'clean-bundle-js'
};
const generatedFileName = 'app.bundle.js';

function initialize(sources, dstPath) {
    if (!sources) { throw 'Sources object is required.'; }
    if (!dstPath) { throw 'Destination Path is required.'; }

    const _ = require('lodash');
    const path = require('path');
    const fse = require('fs-extra');
    const merge = require('merge2');
    const gulp = require('gulp');
    const concat = require('gulp-concat');
    const rjs = require('gulp-requirejs');
    const uglify = require('gulp-uglify');
    const del = require('del');

    const generatedFilePath = path.join(dstPath, generatedFileName);
    const rjsOptionPresets = sources.modules ? {
        baseUrl: _.get(sources, 'modules.baseUrl'),
        name: _.get(sources, 'modules.main'),
        out: generatedFileName
    } : null;
    const rjsOptionOverrides = _.get(sources, 'modules.rjsOptionOverrides');
    const rjsOptions = rjsOptionOverrides ? Object.assign(rjsOptionOverrides, rjsOptionPresets) : rjsOptionPresets;

    gulp.task(tasks.main, function () {
        const prependScriptPaths = sources.prependScriptPaths || [];
        const scriptStream = merge(prependScriptPaths.map(spath => gulp.src(spath)));
        const rjsStream = rjsOptions ? rjs(rjsOptions) : null;
        const sourceStream = rjsStream ? merge(scriptStream, rjsStream) : scriptStream;

        return sourceStream
            .pipe(concat(generatedFileName))
            .pipe(uglify())
            .pipe(gulp.dest(dstPath));
    });
    gulp.task(tasks.noop, function () {
        return fse.pathExists(generatedFilePath)
            .then(function (exists) {
                if (!exists) {
                    return fse.outputFile(generatedFilePath, "// JS bundling is disabled.");
                }
            });
    });
    gulp.task(tasks.watchNoop, function () {
        if (sources.watchPath) {
            gulp.watch(sources.watchPath, { ignoreInitial: false }, gulp.series(tasks.noop));
        }
    });
    gulp.task(tasks.clean, function () {
        return del([
            `${generatedFilePath}*`
        ]);
    });
}

module.exports = initialize;
module.exports.tasks = tasks;
