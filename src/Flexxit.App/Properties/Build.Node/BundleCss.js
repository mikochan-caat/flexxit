const tasks = {
    main: 'bundle-css',
    noop: 'bundle-css-noop',
    watchNoop: 'watch-bundle-css-noop',
    clean: 'clean-bundle-css'
};
const generatedFileName = 'app.bundle.css';

function initialize(srcPath, dstPath) {
    if (!srcPath) { throw 'Source Path is required.'; }
    if (!dstPath) { throw 'Destination Path is required.'; }

    const path = require('path');
    const fse = require('fs-extra');
    const gulp = require('gulp');
    const cleanCss = require('gulp-clean-css');
    const concat = require('gulp-concat');
    const del = require('del');

    const srcPathGlob = `${srcPath}**/*.css`;
    const generatedFilePath = path.join(dstPath, generatedFileName);
    
    gulp.task(tasks.main, function () {
        return gulp.src(srcPathGlob)
            .pipe(cleanCss({
                relativeTo: dstPath
            }))
            .pipe(concat(generatedFileName))
            .pipe(gulp.dest(dstPath));
    });
    gulp.task(tasks.noop, function () {
        return fse.pathExists(generatedFilePath)
            .then(function (exists) {
                if (!exists) {
                    return fse.outputFile(generatedFilePath, "/* CSS bundling is disabled. */");
                }
            });
    });
    gulp.task(tasks.watchNoop, function () {
        gulp.watch(srcPathGlob, { ignoreInitial: false }, gulp.series(tasks.noop));
    });
    gulp.task(tasks.clean, function () {
        return del([
            `${generatedFilePath}*`
        ]);
    });
}

module.exports = initialize;
module.exports.tasks = tasks;
