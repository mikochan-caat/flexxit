/// <binding ProjectOpened='all:watch' />

const gulp = require('gulp');
const typescript = require('./Properties/Build.Node/TypeScript');
const sass = require('./Properties/Build.Node/Sass');
const bundleCss = require('./Properties/Build.Node/BundleCss');
const bundleJs = require('./Properties/Build.Node/BundleJs');

const wwwroot = './wwwroot/';
const wwwrootCss = `${wwwroot}Css/`;
const wwwrootJs = `${wwwroot}Js/`;
const wwwrootBundles = `${wwwroot}Bundles/`;

const tscOptionOverrides = {
};
const sassOptions = {
    outputStyle: 'expanded'
};
typescript('./Content/TypeScript/tsconfig.json', wwwrootJs, tscOptionOverrides);
sass('./Content/Sass/**/*.scss', wwwrootCss, sassOptions);

const bundleJsSources = {
    prependScriptPaths: [
        `${wwwrootJs}Vendor/**/*.js`,
        `${wwwrootJs}*.generated.r.js`
    ],
    modules: {
        baseUrl: wwwrootJs,
        main: 'Compiled/Optimize.Main',
        rjsOptionOverrides: {
        }
    },
    watchPath: wwwrootJs 
};
bundleCss(wwwrootCss, wwwrootBundles);
bundleJs(bundleJsSources, wwwrootBundles);

gulp.task('all:watch', gulp.parallel(sass.tasks.watch, typescript.tasks.watch, bundleCss.tasks.watchNoop, bundleJs.tasks.watchNoop));
gulp.task('all:clean', gulp.series(sass.tasks.clean, typescript.tasks.clean, bundleCss.tasks.clean, bundleJs.tasks.clean));

const buildTasks = ['all:clean', sass.tasks.main, typescript.tasks.main, typescript.tasks.rjs];
gulp.task('build:Debug', gulp.series(...buildTasks, bundleCss.tasks.noop, bundleJs.tasks.noop));
gulp.task('build:Release', gulp.series(...buildTasks, bundleCss.tasks.main, bundleJs.tasks.main));
gulp.task('default', gulp.series('build:Debug'));
