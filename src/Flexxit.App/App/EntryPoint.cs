using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Flexxit.App
{
    public static class EntryPoint
    {
        private static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args)
            => WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
