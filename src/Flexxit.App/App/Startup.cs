using System;
using System.Diagnostics.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleInjector;

namespace Flexxit.App
{
    public sealed class Startup : IDisposable
    {
        private readonly Container container;
        private readonly IHostingEnvironment hostingEnvironment;

        private bool IsDevEnvironment => this.hostingEnvironment.IsDevelopment();

        public Startup(IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            Contract.Requires(configuration != null);

            this.container = new Container();
            this.hostingEnvironment = hostingEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            this.container.GetType();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            Contract.Requires(loggerFactory != null);

            if (this.IsDevEnvironment) {
                app.UseDeveloperExceptionPage();
            }

            app.UseCookiePolicy();
            app.UseResponseCompression();
        }

        public void Dispose()
        {
            this.container.Dispose();
        }
    }
}
